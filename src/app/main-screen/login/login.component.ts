import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/services/user';
import { CookieHelper } from 'src/app/libs/cookie-helper';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public errorMessage: String;

    constructor(private userService: UserService,
        private router: Router) {
        this.errorMessage = null;
    }

    public ngOnInit(): void {
    }

    public login(email, password): void {
        document.getElementById('main-spinner').classList.toggle('hidden');
        this.userService.login(email, password).then(response => {
            document.getElementById('main-spinner').classList.toggle('hidden');
            this.userService.user = User.fromJSON(JSON.parse(JSON.stringify(response)));
            CookieHelper.setCookie('token', this.userService.user.token, 1);
            this.router.navigate(['']);
            this.errorMessage = null;
        }, error => {
            document.getElementById('main-spinner').classList.toggle('hidden');
            this.errorMessage = "CAPTION_ERROR_LOGIN";
        });
    }
}
