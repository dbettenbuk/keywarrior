import { Component, OnInit } from '@angular/core';
import { StatService } from 'src/app/services/stat.service';
import { ScoreRank } from 'src/app/libs/game-info';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  public percentage: number;

  public ScoreRank = ScoreRank;

  public get scoreRank(): ScoreRank {
    return this.statService.summary.scoreRank;
  }

  public get starCount(): number {
    return this.statService.summary.stars;
  }

  public sequence(count) {
    return Array.from(Array(count)).map((x, i) => i);
  }

  public leveledUp: boolean;
  public newLevel: number;

  constructor(
    public userService: UserService,
    public statService: StatService,
    public router: Router
  ) { }

  ngOnInit() {
    document.body.classList.remove('menu');
    document.body.classList.remove('game');
    document.body.classList.add('summary');

    this.leveledUp = false;

    document.getElementById('main-spinner').classList.toggle('hidden');
    this.userService.validateScore(this.statService.score).then((response) => {
      const json = JSON.parse(JSON.stringify(response));
      if (json['leveledUp'] == true) {
        this.leveledUp = true;
        this.newLevel = parseInt(json['newLevel']);
      }
      document.getElementById('main-spinner').classList.toggle('hidden');
    }, (error) => {
      document.getElementById('main-spinner').classList.toggle('hidden');
    });
  }

  public closeSummary(): void {
    document.getElementById('main-spinner').classList.toggle('hidden');
    this.userService.addGame(this.statService.summary).then(response => {
      document.getElementById('main-spinner').classList.toggle('hidden');
      this.router.navigate(['/']);
    }, error => {
      document.getElementById('main-spinner').classList.toggle('hidden');
      console.log(error);
      this.router.navigate(['/']);
    });
  }
}
