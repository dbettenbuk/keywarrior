import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Language } from '../services/language';
import { LanguageService } from '../services/language.service';
import { ViewEncapsulation } from '@angular/compiler/src/core';
import { UserService } from '../services/user.service';
import { CookieHelper } from '../libs/cookie-helper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.scss']
})
export class MainScreenComponent implements OnInit {

  public languageListShown = false;
  public userMenuShown = false;

  @ViewChild('languageList')
  private languageList: ElementRef<HTMLDivElement>;

  @ViewChild('userMenu')
  private userMenu: ElementRef<HTMLDivElement>;

  public get currentLanguage(): Language {
    return this.languageService.currentLanguage;
  }

  public get languages(): any {
    return Language.COLLECTION;
  }

  constructor(private languageService: LanguageService,
    public userService: UserService,
    private router: Router) { }

  public ngOnInit(): void {
    document.body.classList.remove('game');
    document.body.classList.add('menu');
  }

  public showLanguageList(): void {
    this.languageList.nativeElement.classList.toggle('hidden');
    this.languageListShown = !this.languageList.nativeElement.classList.contains('hidden');
  }

  public selectLanguage(language: Language): void {
    this.languageService.use(language);
    this.languageList.nativeElement.classList.toggle('hidden');
    this.languageListShown = !this.languageList.nativeElement.classList.contains('hidden');
  }

  public showUserMenu(): void {
    this.userMenu.nativeElement.classList.toggle('hidden');
    this.userMenuShown = !this.userMenu.nativeElement.classList.contains('hidden');
  }

  public logout(): void {
    document.getElementById('main-spinner').classList.toggle('hidden');
    this.userService.logout().then(response => {
      document.getElementById('main-spinner').classList.toggle('hidden');
      this.userService.user = null;
      CookieHelper.setCookie('token', 'none', 1);
      location.reload();
    }, error => {
      document.getElementById('main-spinner').classList.toggle('hidden');
      location.reload();
    });
  }

}
