import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from 'src/app/services/game.service';
import { LanguageService } from 'src/app/services/language.service';
import { ArticleService } from 'src/app/services/article.service';
import { Article } from 'src/app/libs/article';
import { WikiParser } from 'src/app/libs/wiki-parser';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TextPreviewDialogComponent } from 'src/app/dialogs/text-preview-dialog/text-preview-dialog.component';
import { ErrorDialogComponent } from 'src/app/dialogs/error-dialog/error-dialog.component';
import { NoContentError } from 'src/app/libs/errors/no-content-error';
import { EmptyContentError } from 'src/app/libs/errors/empty-content-error';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @ViewChild('linkInput')
  private linkInputElement: ElementRef<HTMLInputElement>;

  @ViewChild('articleList')
  private articleList: ElementRef<HTMLUListElement>;

  private _articles;

  public get articles(): Array<Article> {
    return this._articles;
  }

  constructor(
    private router: Router,
    private gameService: GameService,
    private languageService: LanguageService,
    private articleService: ArticleService,
    private wikiParser: WikiParser,
    private dialog: MatDialog) { }

  ngOnInit() {
    document.getElementById('main-spinner').classList.toggle('hidden');
    this.articleService.getAllArticles().then((articles) => {
      this._articles = articles;
      document.getElementById('main-spinner').classList.toggle('hidden');
    }, (error) => {
      document.getElementById('main-spinner').classList.toggle('hidden');
    });
  }

  public previewText(): void {
    let url = this.linkInputElement.nativeElement.value.split('/');
    let textTitle = url[url.length - 1].replace(new RegExp('_', 'g'), ' ');
    let layoutKeyPrefix = this.gameService.options.keyboardLayout.split('-')[0];
    this.wikiParser.parseArticle(layoutKeyPrefix, url[url.length - 1], (text, error) => {
      if (error) {
        let message: string;
        if (error instanceof NoContentError) {
          message = 'CAPTION_WIKI_ERROR';
        }
        else if (error instanceof EmptyContentError) {
          message = 'CAPTION_WIKI_LAYOUT_ERROR';
        }
        this.dialog.open(ErrorDialogComponent, {
          width: '500px',
          data: {
            message: message
          }
        });
      }
      else {
        this.dialog.open(TextPreviewDialogComponent, {
          width: '500px',
          data: {
            text: text,
            textTitle: textTitle
          }
        }).afterClosed().subscribe((result) => {
          if (result) {
            this.gameService.setup(text, textTitle, () => {
              window.localStorage.removeItem('timeLimit');
              this.router.navigateByUrl('/game');
            });
          }
        });
      }
    });
  }

  public selectArticle($event) {
    for (let i = 0; i < this.articleList.nativeElement.children.length; i++) {
      this.articleList.nativeElement.children[i].classList.remove('selected');
    }

    const listElement = <HTMLElement>event.currentTarget;
    listElement.classList.add('selected');

    this.linkInputElement.nativeElement.value = this.articles.find((article) => article.name === listElement.querySelector('.article-name').innerHTML).url;

  }
}
