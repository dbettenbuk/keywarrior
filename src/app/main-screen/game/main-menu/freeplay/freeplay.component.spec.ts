import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeplayComponent } from './freeplay.component';

describe('FreeplayComponent', () => {
  let component: FreeplayComponent;
  let fixture: ComponentFixture<FreeplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
