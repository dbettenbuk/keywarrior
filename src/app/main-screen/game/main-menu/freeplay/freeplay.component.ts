import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WikiParser } from 'src/app/libs/wiki-parser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-freeplay',
  templateUrl: './freeplay.component.html',
  styleUrls: ['./freeplay.component.scss']
})
export class FreeplayComponent implements OnInit {

  private difficultyCap: number;

  constructor() {
    this.difficultyCap = 10;
  }

  ngOnInit() {
    this.setDifficulty('1');
  }

  public setDifficulty(difficulty: string): void {
    window.localStorage.setItem('difficulty', difficulty);

    let diffValue = parseInt(difficulty);
    window.localStorage.setItem('keyAnimationTime', (this.difficultyCap - diffValue * 3).toString());
  }
}
