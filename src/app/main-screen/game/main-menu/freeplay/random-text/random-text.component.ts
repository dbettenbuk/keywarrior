import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TextPreviewDialogComponent } from 'src/app/dialogs/text-preview-dialog/text-preview-dialog.component';
import { GameService } from '../../../../../services/game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-random-text',
  templateUrl: './random-text.component.html',
  styleUrls: ['./random-text.component.scss']
})
export class RandomTextComponent implements OnInit {

  private readonly MIN_WORD_LENGTH = 3;
  private readonly MAX_WORD_LENGTH = 10;

  private readonly NUMBER_CHANCE = 6;
  private readonly SPECIAL_CHANCE = 10;

  public generatedText: string;

  constructor(
    private gameService: GameService,
    private router: Router,
    private dialog: MatDialog
  ) {
    this.generatedText = '';
  }

  ngOnInit() { }

  public generateText(length: number, generateNumbers: boolean, generateSpecial: boolean): void {
    this.generatedText = '';

    console.log(generateNumbers + " " + generateSpecial);

    let upperCase = false;
    this.generatedText += String.fromCharCode(this.randomBetween(97, 122)).toUpperCase();
    for (let i = 0; i < length; i++) {
      let chance = this.randomBetween(0, 100);
      if (generateNumbers && chance <= this.NUMBER_CHANCE) {
        this.generatedText += String.fromCharCode(this.randomBetween(48, 57));
      }
      else {
        let character = String.fromCharCode(this.randomBetween(97, 122));
        if (upperCase) {
          character = character.toUpperCase();
          upperCase = false;
        }
        this.generatedText += character;
      }
    }

    let wordLength = this.randomBetween(this.MIN_WORD_LENGTH, this.MAX_WORD_LENGTH);;
    let wordLengthCounter = 0;
    for (let i = 0; i < this.generatedText.length; i++) {
      if (wordLengthCounter === wordLength) {
        let chance = this.randomBetween(0, 100);
        if (chance <= this.SPECIAL_CHANCE) {
          let code = this.randomBetween(44, 46);
          if (code === 45) {
            code = 44;
          }
          this.generatedText = this.insert(this.generatedText, String.fromCharCode(code), i);
        }

        this.generatedText = this.insert(this.generatedText, ' ', i + 1);
        wordLength = this.randomBetween(this.MIN_WORD_LENGTH, this.MAX_WORD_LENGTH);
        wordLengthCounter = 0;
      }
      else {
        wordLengthCounter++;
      }
    }

    this.generatedText = this.generatedText.replace(/.$/, '.');

    this.showPreview();
  }

  private showPreview() {
    this.dialog.open(TextPreviewDialogComponent, {
      width: '500px',
      data: {
        text: this.generatedText,
        textTitle: 'Random text'
      }
    }).afterClosed().subscribe((result) => {
      if (result) {
        this.gameService.setup(this.generatedText, 'Random text', () => {
          window.localStorage.removeItem('timeLimit');
          this.router.navigateByUrl('/game');
        });
      }
    });
  }

  private randomBetween(lower: number, upper: number): number {
    return Math.floor(Math.random() * (upper - lower + 1) + lower);
  }

  private insert(target: string, source: string, index: number): string {
    return target.substr(0, index) + source + target.substr(index);
  }
}
