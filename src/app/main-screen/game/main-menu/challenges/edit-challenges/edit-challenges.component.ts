import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Challenge } from 'src/app/libs/challenge';
import { ChallengeService } from 'src/app/services/challenge.service';
import { MatDialog } from '@angular/material';
import { EditChallengeDialogComponent } from 'src/app/dialogs/edit-challenge-dialog/edit-challenge-dialog.component';
import { AssetController } from 'src/app/libs/asset-controller';
import { KeyValuePair } from 'src/app/libs/key-value-pair';
import { TextPreviewDialogComponent } from 'src/app/dialogs/text-preview-dialog/text-preview-dialog.component';
import { Router } from '@angular/router';
import { GameService } from 'src/app/services/game.service';
import { ConfirmDialogComponent } from 'src/app/dialogs/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from 'src/app/dialogs/error-dialog/error-dialog.component';
import { Parser } from 'src/app/libs/parser';

@Component({
  selector: 'app-edit-challenges',
  templateUrl: './edit-challenges.component.html',
  styleUrls: ['./edit-challenges.component.scss']
})
export class EditChallengesComponent implements OnInit {

  public challenges: Array<Challenge>;

  public get layouts(): Array<KeyValuePair<string, string>> {
    return this.assetController.KeyboardLayouts;
  }

  constructor(
    private parser: Parser,
    private challengeService: ChallengeService,
    private gameService: GameService,
    private assetController: AssetController,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.getChallenges();
  }

  private getChallenges(): void {
    document.getElementById('main-spinner').classList.toggle('hidden');
    this.challengeService.getChallengesForCurrentUser().then((challenges) => {
      this.challenges = challenges;
      document.getElementById('main-spinner').classList.toggle('hidden');
    }, (error) => {
      console.error(error);
      document.getElementById('main-spinner').classList.toggle('hidden');
    });
  }

  public openPreview(challenge: Challenge) {
    this.dialog.open(TextPreviewDialogComponent, {
      width: '500px',
      data: {
        text: challenge.text,
        textTitle: challenge.name
      }
    }).afterClosed().subscribe((result) => {
      if (result) {
        this.gameService.setup(challenge.text, challenge.name, () => {
          window.localStorage.setItem('timeLimit', challenge.timeLimit.toString());
          this.router.navigateByUrl('/game');
        });
      }
    });
  }

  public openChallengeDialog(challenge: Challenge) {
    this.dialog.open(EditChallengeDialogComponent, {
      width: '500px',
      data: {
        challenge: challenge ? challenge : Challenge.createEmpty()
      }
    }).afterClosed().subscribe((result) => {
      if (result && result.confirm) {
        document.getElementById('main-spinner').classList.toggle('hidden');
        result.challenge.text = this.parser.parseText(result.challenge.text);
        if (challenge) {
          this.challengeService.modifyChallenge(result.challenge).then((response) => {
            document.getElementById('main-spinner').classList.toggle('hidden');
            this.getChallenges();
          }, (error) => {
            document.getElementById('main-spinner').classList.toggle('hidden');
            this.dialog.open(ErrorDialogComponent, {
              width: '500px',
              data: {
                message: 'CAPTION_UNKNOWN_ERROR'
              }
            });
          });
        }
        else {
          this.challengeService.addChallenge(result.challenge).then((response) => {
            document.getElementById('main-spinner').classList.toggle('hidden');
            this.getChallenges();
          }, (error) => {
            document.getElementById('main-spinner').classList.toggle('hidden');
            this.dialog.open(ErrorDialogComponent, {
              width: '500px',
              data: {
                message: 'CAPTION_UNKNOWN_ERROR'
              }
            });
          });
        }
      }
    });
  }

  public openChallengeDeleteDialog(challengeId) {
    this.dialog.open(ConfirmDialogComponent, {
      width: '500px'
    }).afterClosed().subscribe((result) => {
      if (result) {
        document.getElementById('main-spinner').classList.toggle('hidden');
        this.challengeService.deleteChallenge(challengeId).then((response) => {
          document.getElementById('main-spinner').classList.toggle('hidden');
          this.getChallenges();
        }, (error) => {
          document.getElementById('main-spinner').classList.toggle('hidden');
          this.dialog.open(ErrorDialogComponent, {
            width: '500px',
            data: {
              message: 'CAPTION_UNKNOWN_ERROR'
            }
          });
        });
      }
    });
  }

}
