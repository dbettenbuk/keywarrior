import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChallengesComponent } from './edit-challenges.component';

describe('EditChallengesComponent', () => {
  let component: EditChallengesComponent;
  let fixture: ComponentFixture<EditChallengesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChallengesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChallengesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
