import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomChallengesComponent } from './custom-challenges.component';

describe('CustomChallengesComponent', () => {
  let component: CustomChallengesComponent;
  let fixture: ComponentFixture<CustomChallengesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomChallengesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomChallengesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
