import { Component, OnInit } from '@angular/core';
import { TextPreviewDialogComponent } from 'src/app/dialogs/text-preview-dialog/text-preview-dialog.component';
import { MatDialog } from '@angular/material';
import { ChallengeService } from 'src/app/services/challenge.service';
import { GameService } from 'src/app/services/game.service';
import { Router } from '@angular/router';
import { Challenge } from 'src/app/libs/challenge';

@Component({
  selector: 'app-custom-challenges',
  templateUrl: './custom-challenges.component.html',
  styleUrls: ['./custom-challenges.component.scss']
})
export class CustomChallengesComponent implements OnInit {

  public challenges: Array<Challenge>;

  constructor(
    private challengeService: ChallengeService,
    private gameService: GameService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    document.getElementById('main-spinner').classList.toggle('hidden');
    this.challengeService.getCustomChallenges().then((challenges) => {
      this.challenges = challenges;
      document.getElementById('main-spinner').classList.toggle('hidden');
    }, (error) => {
      console.error(error);
      document.getElementById('main-spinner').classList.toggle('hidden');
    });
  }

  public openPreview(challenge: Challenge) {
    this.dialog.open(TextPreviewDialogComponent, {
      width: '500px',
      data: {
        text: challenge.text,
        textTitle: challenge.name
      }
    }).afterClosed().subscribe((result) => {
      if (result) {
        this.gameService.setup(challenge.text, challenge.name, () => {
          window.localStorage.setItem('timeLimit', challenge.timeLimit.toString());
          this.router.navigateByUrl('/game');
        });
      }
    });
  }

}
