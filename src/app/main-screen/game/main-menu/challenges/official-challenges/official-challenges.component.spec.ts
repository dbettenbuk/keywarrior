import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficialChallengesComponent } from './official-challenges.component';

describe('OfficialChallengesComponent', () => {
  let component: OfficialChallengesComponent;
  let fixture: ComponentFixture<OfficialChallengesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficialChallengesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficialChallengesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
