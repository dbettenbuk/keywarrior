import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SelectDialogComponent } from 'src/app/dialogs/select-dialog/select-dialog.component';
import { KeyValuePair } from 'src/app/libs/key-value-pair';
import { GameService } from '../../../services/game.service';
import { AssetController } from '../../../libs/asset-controller';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
  public selectedKeyboardSkin: KeyValuePair<string, string>;
  public selectedLayout: KeyValuePair<string, string>;

  constructor(
    private dialog: MatDialog,
    private gameService: GameService,
    private assetController: AssetController) { }

  public ngOnInit(): void {
    this.selectedKeyboardSkin = this.assetController.KeyboardSkins[0];
    this.selectedLayout = this.assetController.KeyboardLayouts[0];

    this.gameService.setOptions({
      keyboardLayout: this.selectedLayout.key,
      keyboardSkin: this.selectedKeyboardSkin.key
    });
  }

  public openDialog(option: number) {
    let items = this.assetController.KeyboardSkins;

    if (option === 2) {
      items = this.assetController.KeyboardLayouts;
    }

    this.dialog.open(SelectDialogComponent, {
      width: '500px',
      data: {
        items: items
      }
    }).afterClosed().subscribe((result) => {
      if (result.confirm && result.selected) {
        if (option === 1) {
          this.selectedKeyboardSkin = result.selected;
        }
        else if (option === 2) {
          this.selectedLayout = result.selected;
        }

        this.gameService.setOptions({
          keyboardLayout: this.selectedLayout.key,
          keyboardSkin: this.selectedKeyboardSkin.key
        });
      }
    });
  }

}
