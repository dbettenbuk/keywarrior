import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pause-menu',
  templateUrl: './pause-menu.component.html',
  styleUrls: ['./pause-menu.component.scss']
})
export class PauseMenuComponent implements OnInit {

  constructor(private gameService: GameService,
    private router: Router) { }

  ngOnInit() {
  }

  public close(): void {
    document.getElementById('pause-menu-panel').classList.add('hidden');
    this.gameService.paused = false;
    Array.from(document.querySelectorAll('.paused')).forEach(element => {
      element.classList.remove('paused');
    });
  }

  public exit(): void {
    this.router.navigateByUrl('/');
  }

}
