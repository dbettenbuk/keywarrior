import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, HostListener } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { KeyFactory } from 'src/app/libs/key-factory';
import { Key, KeyType } from 'src/app/libs/key';
import { StatService } from 'src/app/services/stat.service';
import { Router } from '@angular/router';
import { ScoreHelper } from 'src/app/libs/score-helper';
import { GameInfo, ScoreRank } from 'src/app/libs/game-info';
import { UserService } from 'src/app/services/user.service';

enum KeyRemoveReason {
  HIT,
  EXPIRED
}

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './game.component.scss',
    './keyboard-layouts/animations.scss',
    './keyboard-layouts/common.scss',
    './keyboard-layouts/en-gb.scss',
    './keyboard-layouts/hu-hu.scss'
  ]
})
export class GameComponent implements OnInit {

  @ViewChild('gameboard')
  private gameboard: ElementRef<HTMLDivElement>;

  @ViewChild('textIndicator')
  private textIndicator: ElementRef<HTMLPreElement>;

  @ViewChild('pauseButton')
  private pauseButton: ElementRef<HTMLButtonElement>;

  private keyFactory: KeyFactory;
  private keysOnScreen: Array<Key>;
  private letterCounter: number;
  private currentLetterCounter: number;
  private generatorTimer;
  private keyAnimationTime: number;
  private timeLimit: number;
  private timeCounter: number;
  private elapsedTimer;
  private cpmTimer;
  private cpmCounter: number;
  private _currentStreak: number;

  private cpmData: Array<number>;
  private measureCount: number;

  private streak: number;
  private _multiplier: number;

  private countMiss: boolean;

  private _finished: boolean;

  public effectiveCpm: number;

  public get currentText(): string {
    return this.gameService.currentText;
  }

  public get finished(): boolean {
    return this._finished;
  }

  public get score(): number {
    return this.statService.score;
  }

  public get multiplier(): number {
    return this._multiplier;
  }

  public get correctHits(): number {
    return this.statService.correctHits;
  }

  public get wrongHits(): number {
    return this.statService.wrongHits;
  }

  public get currentStreak(): number {
    return this._currentStreak;
  }

  constructor(
    private userService: UserService,
    private gameService: GameService,
    private statService: StatService,
    private router: Router) {

    this.statService.reset();
    this.keyFactory = new KeyFactory();
    this.gameService.paused = false;
    this.countMiss = true;
    this._finished = false;
    this.currentLetterCounter = 0;
    this.keysOnScreen = new Array<Key>();
    this.keyAnimationTime = 6;
    this._multiplier = 1;
    this.streak = 0;
    this.timeCounter = 0;
    this.timeLimit = 0;
    this.cpmCounter = 0;
    this._currentStreak = 0;
    this.measureCount = 30;
    this.cpmData = Array<number>();

    let timeLimit = window.localStorage.getItem('timeLimit');

    if (timeLimit) {
      this.timeLimit = parseInt(timeLimit);
      this.timeCounter = this.timeLimit;
    }

    this.elapsedTimer = setInterval(this.measureTime.bind(this), 1000);
    this.cpmTimer = setInterval(this.measureCpm.bind(this), 1000);
  }

  ngOnInit() {
    document.body.classList.remove('menu');
    document.body.classList.add('game');
    this.keyAnimationTime = parseInt(window.localStorage.getItem('keyAnimationTime'));

    this.keyFactory.createKeyboard(this.keyFactory.getCodesFor(this.gameService.options.keyboardLayout), this.gameService.options.keyboardSkin)
      .forEach((key) => {
        this.gameboard.nativeElement.appendChild(key.element);
        key.element.classList.add(this.gameService.options.keyboardLayout);
      });

    this.letterCounter = 0;

    this.generatorTimer = setInterval(this.generateNext.bind(this), 1000);
  }

  private measureCpm(): void {
    if (this.cpmData.length >= this.measureCount) {
      this.cpmData.shift();
    }
    this.cpmData.push(this.cpmCounter * 60);

    this.effectiveCpm = 0;
    this.cpmData.forEach((x) => {
      this.effectiveCpm += x;
    });
    this.effectiveCpm = Math.floor(this.effectiveCpm / this.measureCount);

    this.statService.averageCpm += this.cpmCounter * 60;

    this.cpmCounter = 0;
  }

  private measureTime(): void {
    if (!this.gameService.paused) {
      if (this.timeLimit > 0) {
        this.timeCounter--;
        if (this.timeCounter <= 0) {
          this.pauseAnimations();
          this.gameService.paused = true;
          this.endGame();
        }
      }
      else {
        this.timeCounter++;
      }
    }
  }

  private generateNext(): void {
    if (!this.gameService.paused) {
      let char = this.currentText[this.letterCounter];
      let key = this.keyFactory.createKey(char, KeyType.FLOATING, this.gameService.options.keyboardSkin);
      key.container.classList.add(this.gameService.options.keyboardLayout);
      this.setAnimationEnd(key);
      this.keysOnScreen.push(key);

      key.container.style.animationDuration = this.keyAnimationTime + 's';

      if (this.keysOnScreen.findIndex(x => x === key) === 0) {
        key.element.style['animation'] = 'currentkey-mark 1s infinite';
      }

      this.letterCounter++;

      if (this.letterCounter >= this.currentText.length) {
        clearInterval(this.generatorTimer);
        this.generatorTimer = null;
        this.letterCounter = 0;
      }

      if (this.generatorTimer) {
        clearInterval(this.generatorTimer);
        this.generatorTimer = null;
      }

      this.gameboard.nativeElement.appendChild(key.container);
    }
  }

  @HostListener('document:keydown', ['$event'])
  private keyDown(event: KeyboardEvent) {
    if (!this.gameService.paused) {
      let content = event.key;
      let keyClass = Key.getClassFor(content);
      let backgroundImage = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/key_pressed.png)';

      if (keyClass === 'key-32') {
        backgroundImage = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/space_pressed.png)';
      }

      try {
        let element = <HTMLElement>document.querySelector('.' + keyClass);
        if (element !== null) {
          element.style['background-image'] = backgroundImage;
        }
      } catch (error) {
        console.log('No element found with class ' + keyClass);
      }
    }
  }

  @HostListener('document:keypress', ['$event'])
  private keyPress(event: KeyboardEvent) {
    if (!this.gameService.paused) {
      let content = event.key;
      if (this.keysOnScreen.length > 0) {
        if (content === this.keysOnScreen[0].content) {
          this.statService.correctHits++;
          this.streak++;
          this.statService.score += ScoreHelper.getScoreForChar(content);
          this._currentStreak++;
          this.removeKey(KeyRemoveReason.HIT);
        }
        else {
          if (this.countMiss && content.charCodeAt(0) !== 16) {
            this.statService.wrongHits++;
            this.countMiss = false;
            this.streak = 0;

            if (this._currentStreak > this.statService.highestStreak) {
              this.statService.highestStreak = this._currentStreak;
            }

            this._currentStreak = 0;
          }

        }

        this._multiplier = this.determineMultiplier();
      }

      this.cpmCounter++;
    }
  }

  @HostListener('document:keyup', ['$event'])
  private keyUp(event: KeyboardEvent) {
    let content = event.key;

    let keyClass = Key.getClassFor(content);
    let backgroundImage = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/key.png)';

    if (keyClass === 'key-32') {
      backgroundImage = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/space.png)';
    }

    let element = <HTMLElement>document.querySelector('.' + keyClass);
    if (element !== null) {
      element.style['background-image'] = backgroundImage;
    }
  }

  private removeKey(reason: KeyRemoveReason): void {
    if (reason === KeyRemoveReason.HIT) {
      this.keysOnScreen[0].element.style['animation'] = 'correct-key 1s 1';
      if (this.keysOnScreen[0].content === ' ') {
        this.keysOnScreen[0].element.style['background-image'] = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/space_green.png)';
      }
      else {
        this.keysOnScreen[0].element.style['background-image'] = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/key_green.png)';
      }
    }
    else {
      if (this.countMiss) {
        this.statService.wrongHits++;
      }

      this.keysOnScreen[0].element.style['animation'] = 'missed-key 1s 1';
      if (this.keysOnScreen[0].content === ' ') {
        this.keysOnScreen[0].element.style['background-image'] = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/space_red.png)';
      }
      else {
        this.keysOnScreen[0].element.style['background-image'] = 'url(assets/keyboard/' + this.gameService.options.keyboardSkin + '/key_red.png)';
      }
    }

    let first = this.keysOnScreen.shift();
    this.scheduleForRemove(first);
    first.container.removeEventListener('animationend', this.removeAnimation);
    this.currentLetterCounter++;

    if (this.keysOnScreen.length > 0) {
      this.keysOnScreen[0].element.style['animation'] = 'currentkey-mark 1s infinite';
    }

    console.log(this.currentLetterCounter + ', ' + this.currentText.length);
    if (this.currentLetterCounter >= this.currentText.length) {
      this.endGame();
    } else {
      this.generateNext();
    }

    this.textIndicator.nativeElement.innerHTML = this.textIndicator.nativeElement.innerHTML.slice(1, this.textIndicator.nativeElement.innerHTML.length);

    this.countMiss = true;
  }

  private setAnimationEnd(key: Key): void {
    key.container.addEventListener('animationend', this.removeAnimation.bind(this), false);
  }

  private removeAnimation(): void {
    this.removeKey(KeyRemoveReason.EXPIRED);
  }

  private scheduleForRemove(key: Key): void {
    setTimeout((function () {
      if (this.gameboard.nativeElement.contains(key.container)) {
        this.gameboard.nativeElement.removeChild(key.container);
      }
    }).bind(this), 900);
  }

  public openMenu(): void {
    this.pauseAnimations();
    document.getElementById('pause-menu-panel').classList.remove('hidden');
    this.gameService.paused = true;
  }

  private pauseAnimations(): void {
    for (let key of this.keysOnScreen) {
      if (!key.container.classList.contains('paused')) {
        key.container.classList.add('paused');
      }
    }
  }

  private determineMultiplier(): number {
    let mul = Math.ceil(this.streak / (15 + this.streak * 0.1));
    if (mul > ScoreHelper.MAX_MULTIPLIER) {
      mul = ScoreHelper.MAX_MULTIPLIER;
    }

    return mul;
  }

  public getFormattedTime(): string {
    return Math.floor(this.timeCounter / 60).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + ':' + Math.floor(this.timeCounter % 60).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })
  }

  private endGame(): void {
    this.pauseButton.nativeElement.disabled = true;
    clearInterval(this.elapsedTimer);
    clearInterval(this.cpmTimer);
    if (this.currentLetterCounter < this.currentText.length) {
      this.statService.wrongHits += this.currentText.length - this.currentLetterCounter;
    }
    this.statService.summary = this.createGameData();

    setTimeout((() => { this._finished = true }).bind(this), 2000);
  }

  private createGameData(): GameInfo {
    let difficulty = parseInt(window.localStorage.getItem('difficulty'));
    let elapsedSeconds = this.timeLimit > 0 ? this.timeLimit - this.timeCounter : this.timeCounter;

    return new GameInfo(
      this.statService.score,
      this.gameService.textTitle,
      difficulty,
      this.statService.correctHits,
      this.statService.wrongHits,
      new Date(),
      elapsedSeconds,
      Math.floor(this.statService.averageCpm / elapsedSeconds),
      this.statService.highestStreak,
      this.userService.user.id);
  }
}
