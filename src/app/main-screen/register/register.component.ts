import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { CookieHelper } from 'src/app/libs/cookie-helper';
import { User } from 'src/app/services/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public errorMessage: String;

  constructor(private userService: UserService,
    private router: Router) {
    this.errorMessage = null;
  }

  public ngOnInit(): void {
  }

  public register(username, email, password, confirmPassword): void {
    const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (!emailRegex.test(email)) {
      this.errorMessage = 'CAPTION_ERROR_EMAIL';
    }
    else if (password !== confirmPassword) {
      this.errorMessage = 'CAPTION_ERROR_CONFIRM';
    }
    else {
      document.getElementById('main-spinner').classList.toggle('hidden');
      this.userService.register(username, email, password).then(response => {
        console.log('Successfully registered.');
        this.userService.user = User.fromJSON(JSON.parse(JSON.stringify(response)));
        CookieHelper.setCookie('token', this.userService.user.token, 1);
        document.getElementById('main-spinner').classList.toggle('hidden');
        this.router.navigate(['']);
      }, error => {
        this.errorMessage = 'CAPTION_ERROR_REGISTER';
        console.log('Error on register:\n' + error);
        document.getElementById('main-spinner').classList.toggle('hidden');
      });
    }
  }
}
