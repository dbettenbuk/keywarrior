import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/services/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  public errorMessage: string;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  public modifyAccount(email, password, confirmPassword) {
    const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (!emailRegex.test(email)) {
      this.errorMessage = 'CAPTION_ERROR_EMAIL';
    }
    else if (password !== confirmPassword) {
      this.errorMessage = 'CAPTION_ERROR_CONFIRM';
    }
    else {
      document.getElementById('main-spinner').classList.toggle('hidden');

      this.userService.modifyUser(email, password).then((response) => {
        document.getElementById('main-spinner').classList.toggle('hidden');
        this.errorMessage = null;
      }, (error) => {
        document.getElementById('main-spinner').classList.toggle('hidden');
        this.errorMessage = error;
      });
    }
  }

}
