import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/services/user';
import { MatProgressBar } from '@angular/material/progress-bar';

@Component({
  selector: 'app-account-summary',
  templateUrl: './account-summary.component.html',
  styleUrls: ['./account-summary.component.scss']
})
export class AccountSummaryComponent implements OnInit {

  public stats: {
    level,
    xp,
    xpForCurrentLevel,
    xpForNextLevel,
    highestScore,
    highestStreak,
    bestCpm
  };

  public progressBarValue: number;

  public get user(): User {
    return this.userService.user;
  }

  public get registeredAt() {
    return new Date(this.user.registeredAt).toLocaleDateString();
  }

  constructor(private userService: UserService) {
    this.stats = {
      level: 'N/A',
      xp: 'N/A',
      xpForCurrentLevel: 'N/A',
      xpForNextLevel: 'N/A',
      highestScore: 'N/A',
      highestStreak: 'N/A',
      bestCpm: 'N/A'
    };

    this.progressBarValue = 0;
  }

  ngOnInit() {
    document.getElementById('main-spinner').classList.toggle('hidden');
    this.userService.getStats().then((response) => {
      const json = JSON.parse(JSON.stringify(response));
      this.stats = json;
      this.progressBarValue = (json['xp'] - json['xpForCurrentLevel']) / (json['xpForNextLevel'] - json['xpForCurrentLevel']) * 100;

      document.getElementById('main-spinner').classList.toggle('hidden');
    }, (error) => {
      document.getElementById('main-spinner').classList.toggle('hidden');
    });
  }

}
