import { Component, OnInit } from '@angular/core';
import { GameInfo } from 'src/app/libs/game-info';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {

  public games: Array<GameInfo>;

  public sequence(count) {
    return Array.from(Array(count)).map((x, i) => i);
  }

  constructor(
    private userService: UserService
  ) {
    this.games = Array<GameInfo>();
  }

  ngOnInit() {
    document.getElementById('main-spinner').classList.toggle('hidden');

    this.userService.getAllGames().then((response) => {
      const json = JSON.parse(JSON.stringify(response));

      for (let index in json) {
        this.games.push(new GameInfo(
          json[index].score,
          json[index].textTitle,
          json[index].difficulty,
          json[index].correctHits,
          json[index].wrongHits,
          json[index].playedAt,
          json[index].elapsedTime,
          json[index].averageCpm,
          json[index].highestStreak,
          json[index].userId
        ));
      }

      document.getElementById('main-spinner').classList.toggle('hidden');
    }, (error) => {
      document.getElementById('main-spinner').classList.toggle('hidden');
    });
  }

  public getFormattedTime(time: number): string {
    return Math.floor(time / 60).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + ':' + Math.floor(time % 60).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })
  }

}
