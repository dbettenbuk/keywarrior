import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from './user.service';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public userService: UserService, public router: Router) { }

  public canActivate(): Promise<boolean> {
    document.getElementById('main-spinner').classList.toggle('hidden');
    return this.userService.authenticate().then((response) => {
      this.userService.user = User.fromJSON(JSON.parse(JSON.stringify(response)));
      document.getElementById('main-spinner').classList.toggle('hidden');
      return true;
    }, error => {
      console.log(error);
      document.getElementById('main-spinner').classList.toggle('hidden');
      this.router.navigate(['/login']);
      return false;
    });
  }
}
