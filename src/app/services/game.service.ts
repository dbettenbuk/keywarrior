import { Injectable } from '@angular/core';
import { WikiParser } from '../libs/wiki-parser';
import { Router } from '@angular/router';
import { Language } from './language';
import { ScoreHelper } from '../libs/score-helper';
import { GameOptionsData } from '../libs/game-options-data';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private _currentText: string;
  private _difficulty: number;
  private _paused: boolean;
  private _textTitle: string;
  private _options: GameOptionsData;

  public get currentText(): string {
    return this._currentText;
  }

  public get difficulty(): number {
    return this._difficulty;
  }

  public get paused() {
    return this._paused;
  }

  public set paused(value: boolean) {
    this._paused = value;
  }

  public get textTitle() {
    return this._textTitle;
  }

  public get options(): GameOptionsData {
    return this._options;
  }

  constructor() {
    this._currentText = window.localStorage.getItem('rawText');
    this._textTitle = window.localStorage.getItem('textTitle');
    this._difficulty = parseInt(window.localStorage.getItem('difficulty'));
    this._options = {
      keyboardLayout: window.localStorage.getItem('layout'),
      keyboardSkin: window.localStorage.getItem('skin')
    };
  }

  public setup(text: string, textTitle: string, callback: Function): void {
    this._difficulty = parseInt(window.localStorage.getItem('difficulty'));
    this._currentText = text;
    this._textTitle = textTitle;
    window.localStorage.setItem('rawText', this._currentText);
    window.localStorage.setItem('textTitle', this._textTitle);
    window.localStorage.setItem('layout', this.options.keyboardLayout);
    window.localStorage.setItem('skin', this.options.keyboardSkin);
    callback();
  }

  public setOptions(options: GameOptionsData) {
    this._options = options;
  }
}
