export class Language {

  public static readonly COLLECTION = {
    'ENGLISH': new Language('en', 'English'),
    'HUNGARIAN': new Language('hu', 'Magyar')
  }

  private constructor(public key: string, public fullName: string) { }
}