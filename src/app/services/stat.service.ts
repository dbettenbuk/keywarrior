import { Injectable } from '@angular/core';
import { ScoreRank, GameInfo } from '../libs/game-info';

@Injectable({
  providedIn: 'root'
})
export class StatService {

  private _score: number;
  private _correctHits: number;
  private _wrongHits: number;
  private _highestStreak: number;
  private _scoreRank: ScoreRank;
  private _averageCpm: number;
  private _summary: GameInfo;

  public get score(): number {
    return this._score;
  }

  public set score(value: number) {
    this._score = value;
  }

  public get correctHits(): number {
    return this._correctHits;
  }

  public set correctHits(value: number) {
    this._correctHits = value;
  }

  public get wrongHits(): number {
    return this._wrongHits;
  }

  public set wrongHits(value: number) {
    this._wrongHits = value;
  }

  public get highestStreak(): number {
    return this._highestStreak;
  }

  public set highestStreak(value: number) {
    this._highestStreak = value;
  }

  public get scoreRank(): ScoreRank {
    return this._scoreRank;
  }

  public set scoreRank(value: ScoreRank) {
    this._scoreRank = value;
  }

  public get averageCpm(): number {
    return this._averageCpm;
  }

  public set averageCpm(value: number) {
    this._averageCpm = value;
  }

  public get summary(): GameInfo {
    return this._summary;
  }

  public set summary(value: GameInfo) {
    this._summary = value;
  }

  constructor() {
    this.reset();
  }

  public reset(): void {
    this._score = 0;
    this._correctHits = 0;
    this._wrongHits = 0;
    this._highestStreak = 0;
    this._averageCpm = 0;
  }
}
