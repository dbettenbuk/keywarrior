import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Language } from './language';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  public static readonly DEFAULT_LANGUAGE = Language.COLLECTION['ENGLISH'];

  public data: any = {};
  public currentLanguage: Language;

  constructor(private http: HttpClient) { }

  public use(language: Language): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      const langPath = `assets/locale/${language.key || LanguageService.DEFAULT_LANGUAGE.key}.json`;
      this.http.get<{}>(langPath).subscribe(
        translation => {
          this.data = Object.assign({}, translation || {});
          this.currentLanguage = language;
          resolve(this.data);
        },
        error => {
          this.data = {};
          this.currentLanguage = LanguageService.DEFAULT_LANGUAGE;
          resolve(this.data);
        }
      );
    });
  }
}
