import {
    Injectable
} from '@angular/core';
import {
    HttpClient,
    HttpErrorResponse,
    HttpHeaders
} from '@angular/common/http';
import {
    User
} from './user';
import { CookieHelper } from '../libs/cookie-helper';
import { GameInfo } from '../libs/game-info';

@Injectable()
export class UserService {
    public user: User;

    constructor(private http: HttpClient) { }

    public login(email, password): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.post<User>('/rest/users/login', {
                email: email,
                password: password
            }).subscribe(resolve, reject);
        });
    }

    public register(username, email, password): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.post<User>('/rest/users/register', {
                username: username,
                email: email,
                password: password
            }).subscribe(resolve, reject);
        });
    }

    public authenticate(asAdmin: boolean = false): Promise<{}> {
        return new Promise((resolve, reject) => {
            const headers: HttpHeaders = new HttpHeaders().append('Authorization', 'Bearer ' + CookieHelper.getCookie('token'));
            this.http.post('/rest/users/auth', {}, {
                headers: this.createBearerHeader()
            }).subscribe(resolve, reject);
        });
    }

    public logout(): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.get('/rest/users/logout').subscribe(resolve, reject);
        });
    }

    public modifyUser(email, password): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.post<User>('/rest/users/modify', {
                username: this.user.username,
                email: email,
                password: password
            }, {
                    headers: this.createBearerHeader()
                }).subscribe(resolve, reject);
        });
    }

    public addGame(game: GameInfo): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.post('/rest/users/addgame', {
                gameInfo: game
            }, {
                    headers: this.createBearerHeader()
                }).subscribe(resolve, reject);
        });
    }

    public getAllGames(): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.get('/rest/users/games', {
                headers: this.createBearerHeader()
            }).subscribe(resolve, reject);
        });
    }

    public getStats(): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.get('/rest/users/stats', {
                headers: this.createBearerHeader()
            }).subscribe(resolve, reject);
        });
    }

    public validateScore(score): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.http.post('/rest/users/validatescore', {
                score: score
            }, {
                    headers: this.createBearerHeader()
                }).subscribe(resolve, reject);
        });
    }

    public createBearerHeader(): HttpHeaders {
        return new HttpHeaders().append('Authorization', 'Bearer ' + CookieHelper.getCookie('token'));
    }
}
