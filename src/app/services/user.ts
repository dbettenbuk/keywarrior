export class User {
  public get id(): string {
    return this._id;
  }

  public get registeredAt(): Date {
    return this._registeredAt;
  }

  public get isAdmin(): boolean {
    return this._isAdmin;
  }

  public constructor(
    private _id: string,
    public username: string,
    public email: string,
    public token: string,
    private _registeredAt: Date,
    private _isAdmin: boolean) { }

  public static fromJSON(json: JSON): User {
    return new User(json['_id'], json['username'], json['email'], json['token'], json['registeredAt'], json['isAdmin']);
  }
}
