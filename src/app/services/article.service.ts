import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { Article } from '../libs/article';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private userService: UserService, private http: HttpClient) { }

  public getAllArticles(): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      this.http.get('/rest/articles', {
        headers: this.userService.createBearerHeader()
      }).toPromise().then((response) => {
        const articles = Array<Article>();
        const json = JSON.parse(JSON.stringify(response));
        for (let index in json) {
          articles.push(new Article(
            json[index].name,
            json[index].url,
            json[index].length
          ));
        }
        resolve(articles);
      }, (error) => {
        reject(error);
      });
    });
  }
}
