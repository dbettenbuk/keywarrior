import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';
import { Challenge } from '../libs/challenge';

@Injectable({
  providedIn: 'root'
})
export class ChallengeService {

  constructor(private userService: UserService, private http: HttpClient) { }

  public getOfficialChallenges(): Promise<Array<Challenge>> {
    return new Promise<Array<Challenge>>((resolve, reject) => {
      this.http.get('/rest/challenges/official', {
        headers: this.userService.createBearerHeader()
      }).toPromise().then((response) => {
        resolve(this.readChallengesFromResponse(response));
      }, (error) => {
        reject(error);
      });
    });
  }

  public getCustomChallenges(): Promise<Array<Challenge>> {
    return new Promise<Array<Challenge>>((resolve, reject) => {
      this.http.get('/rest/challenges/custom', {
        headers: this.userService.createBearerHeader()
      }).toPromise().then((response) => {
        resolve(this.readChallengesFromResponse(response));
      }, (error) => {
        reject(error);
      })
    });
  }

  public getChallengesForCurrentUser(): Promise<Array<Challenge>> {
    return new Promise<Array<Challenge>>((resolve, reject) => {
      this.http.get('/rest/challenges', {
        headers: this.userService.createBearerHeader()
      }).toPromise().then((response) => {
        resolve(this.readChallengesFromResponse(response));
      }, (error) => {
        reject(error);
      });
    });
  }

  public modifyChallenge(challenge): Promise<Array<Challenge>> {
    return new Promise<Array<Challenge>>((resolve, reject) => {
      this.http.put('/rest/challenges/modify', {
        challenge: challenge
      }, {
          headers: this.userService.createBearerHeader()
        }).subscribe(resolve, reject);
    });
  }

  public addChallenge(challenge): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      this.http.post('/rest/challenges/add', {
        challenge: challenge
      }, {
          headers: this.userService.createBearerHeader()
        }).subscribe(resolve, reject);
    });
  }

  public deleteChallenge(id): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      this.http.delete('/rest/challenges/delete/' + id, {
        headers: this.userService.createBearerHeader()
      }).subscribe(resolve, reject);
    });
  }

  private readChallengesFromResponse(json): Array<Challenge> {
    const challenges = Array<Challenge>();
    const parsed = JSON.parse(JSON.stringify(json));
    for (let index in parsed) {
      challenges.push(new Challenge(
        parsed[index]._id,
        parsed[index].name,
        parsed[index].text,
        parsed[index].timeLimit,
        parsed[index].difficulty,
        parsed[index].isOfficial,
        parsed[index].user.userName
      ));
    }
    return challenges;
  }

}
