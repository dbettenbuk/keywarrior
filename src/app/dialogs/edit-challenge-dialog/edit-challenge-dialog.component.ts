import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { Challenge } from 'src/app/libs/challenge';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'src/app/services/user.service';

export interface EditChallengeDialogData {
  challenge: Challenge
}

@Component({
  selector: 'app-edit-challenge-dialog',
  templateUrl: './edit-challenge-dialog.component.html',
  styleUrls: ['./edit-challenge-dialog.component.scss']
})
export class EditChallengeDialogComponent implements OnInit {

  @ViewChild('minutesField')
  private minutesField: ElementRef<HTMLInputElement>;

  @ViewChild('secondsField')
  private secondsField: ElementRef<HTMLInputElement>;

  constructor(
    public userService: UserService,
    public dialogRef: MatDialogRef<EditChallengeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditChallengeDialogData
  ) { }

  ngOnInit() {
    this.minutesField.nativeElement.value = Math.floor(this.data.challenge.timeLimit / 60).toString();
    this.secondsField.nativeElement.value = Math.floor(this.data.challenge.timeLimit % 60).toString();
  }

  public setTime() {
    this.data.challenge.timeLimit = parseInt(this.minutesField.nativeElement.value) * 60 + parseInt(this.secondsField.nativeElement.value);
  }

  public close(result: boolean): void {
    this.dialogRef.close({
      confirm: result,
      challenge: this.data.challenge
    });
  }
}
