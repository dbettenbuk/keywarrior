import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChallengeDialogComponent } from './edit-challenge-dialog.component';

describe('EditChallengeDialogComponent', () => {
  let component: EditChallengeDialogComponent;
  let fixture: ComponentFixture<EditChallengeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChallengeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChallengeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
