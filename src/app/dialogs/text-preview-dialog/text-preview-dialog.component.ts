import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface TextPreviewDialogData {
  text: string,
  textTitle: string
}

@Component({
  selector: 'app-text-preview-dialog',
  templateUrl: './text-preview-dialog.component.html',
  styleUrls: ['./text-preview-dialog.component.scss']
})
export class TextPreviewDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<TextPreviewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TextPreviewDialogData) { }

}
