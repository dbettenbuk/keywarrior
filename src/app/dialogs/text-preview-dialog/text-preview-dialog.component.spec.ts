import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextPreviewDialogComponent } from './text-preview-dialog.component';

describe('TextPreviewDialogComponent', () => {
  let component: TextPreviewDialogComponent;
  let fixture: ComponentFixture<TextPreviewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextPreviewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextPreviewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
