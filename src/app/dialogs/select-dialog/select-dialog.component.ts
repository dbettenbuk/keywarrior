import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { KeyValuePair } from 'src/app/libs/key-value-pair';

export interface SelectDialogData {
  items: Array<KeyValuePair<string, string>>;
}

@Component({
  selector: 'app-select-dialog',
  templateUrl: './select-dialog.component.html',
  styleUrls: ['./select-dialog.component.scss']
})
export class SelectDialogComponent {

  public items: Array<KeyValuePair<string, string>>;
  public selectedItem: KeyValuePair<string, string>;

  constructor(
    public dialogRef: MatDialogRef<SelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SelectDialogData) {
    this.items = data.items;
    this.selectedItem = null;
  }

  public close(result: boolean): void {
    this.dialogRef.close({
      confirm: result,
      selected: this.selectedItem
    });
  }

}
