import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {
  @Input()
  public title: string;

  @Input()
  public hasHeader: boolean;

  constructor() { }

  public ngOnInit(): void {
  }

}
