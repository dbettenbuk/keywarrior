import { Component, OnInit, ViewChild, ElementRef, Input, AfterViewInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-select-item',
  templateUrl: './select-item.component.html',
  styleUrls: ['./select-item.component.scss']
})
export class SelectItemComponent implements OnInit, AfterViewInit {

  public activeColorAttribute: string;

  @ViewChild('container')
  private container: ElementRef<HTMLDivElement>;

  private _name: string;

  @Input('name')
  public set name(value: string) {
    this._name = value;
    if (this.container !== undefined && this.container.nativeElement !== undefined) {
      this.container.nativeElement.setAttribute('name', value);
    }
  }

  public get name(): string {
    return this._name;
  }

  @Input('selected')
  public set selected(value: boolean) {
    if (!this.container.nativeElement.classList.contains('selected') && value) {
      this.container.nativeElement.classList.add('selected');
    }
  }

  public get selected(): boolean {
    return this.container.nativeElement.classList.contains('selected');
  }

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    // workaround for a bug when the user refreshes the page
    let routerLink = (<HTMLElement>this.elementRef.nativeElement).getAttribute('routerLink');
    if (routerLink && routerLink !== '' && routerLink !== '/' && window.location.href.includes(routerLink)) {
      this.select();
    }
  }

  public selectEvent($event) {
    this.select();
  }

  private select() {
    const elements = document.querySelectorAll('.select-item-container[name="' + this.name + '"]');
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].classList.contains('selected')) {
        elements[i].classList.remove('selected');
      }
    }

    this.selected = true;
  }

}
