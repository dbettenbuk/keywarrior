import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainScreenComponent } from '../main-screen/main-screen.component';
import { UserPanelComponent } from '../main-screen/user-panel/user-panel.component';
import { LoginComponent } from '../main-screen/login/login.component';
import { RegisterComponent } from '../main-screen/register/register.component';
import { MainMenuComponent } from '../main-screen/game/main-menu/main-menu.component';
import { AuthGuardService } from '../services/auth-guard.service';
import { ChallengesComponent } from '../main-screen/game/main-menu/challenges/challenges.component';
import { FreeplayComponent } from '../main-screen/game/main-menu/freeplay/freeplay.component';
import { ArticleComponent } from '../main-screen/game/main-menu/freeplay/article/article.component';
import { RandomTextComponent } from '../main-screen/game/main-menu/freeplay/random-text/random-text.component';
import { GameComponent } from '../main-screen/game/game.component';
import { AccountSummaryComponent } from '../main-screen/user-panel/account-summary/account-summary.component';
import { EditComponent } from '../main-screen/user-panel/edit/edit.component';
import { GamesComponent } from '../main-screen/user-panel/games/games.component';
import { OfficialChallengesComponent } from '../main-screen/game/main-menu/challenges/official-challenges/official-challenges.component';
import { CustomChallengesComponent } from '../main-screen/game/main-menu/challenges/custom-challenges/custom-challenges.component';
import { EditChallengesComponent } from '../main-screen/game/main-menu/challenges/edit-challenges/edit-challenges.component';

const routes: Routes = [
  {
    path: '',
    component: MainScreenComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'user',
        component: UserPanelComponent,
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            component: AccountSummaryComponent
          },
          {
            path: 'edit',
            component: EditComponent
          },
          {
            path: 'games',
            component: GamesComponent
          }
        ]
      },
      {
        path: '',
        component: MainMenuComponent,
        canActivate: [AuthGuardService],
        children: [
          {
            path: 'challenges',
            component: ChallengesComponent,
            children: [
              {
                path: '',
                component: OfficialChallengesComponent
              },
              {
                path: 'custom',
                component: CustomChallengesComponent
              },
              {
                path: 'edit',
                component: EditChallengesComponent
              }
            ]
          },
          {
            path: '',
            component: FreeplayComponent,
            children: [
              {
                path: '',
                component: ArticleComponent
              },
              {
                path: 'random-text',
                component: RandomTextComponent
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: 'game',
    canActivate: [AuthGuardService],
    component: GameComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
