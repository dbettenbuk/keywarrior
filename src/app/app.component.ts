import {
  Component, ViewChild, ElementRef, OnInit
} from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { LanguageService } from './services/language.service';
import { Language } from './services/language';
import { UserService } from './services/user.service';
import { User } from './services/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public title = 'KeyWarrior';

  constructor(
    private router: Router,
    private userService: UserService) {

  }

  public ngOnInit(): void {

  }
}
