import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { BackgroundComponent } from './background/background.component';
import { FloatingKeyComponent } from './background/floating-key/floating-key.component';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { UserPanelComponent } from './main-screen/user-panel/user-panel.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginComponent } from './main-screen/login/login.component';
import { RegisterComponent } from './main-screen/register/register.component';
import { PanelComponent } from './elements/panel/panel.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './services/user.service';
import { MainMenuComponent } from './main-screen/game/main-menu/main-menu.component';
import { LanguageService } from './services/language.service';
import { TranslatePipe } from './pipes/translate.pipe';
import { ClickOutsideModule } from 'ng-click-outside';
import { AuthGuardService } from './services/auth-guard.service';
import { SplitPaneComponent } from './elements/split-pane/split-pane.component';
import { ChallengesComponent } from './main-screen/game/main-menu/challenges/challenges.component';
import { FreeplayComponent } from './main-screen/game/main-menu/freeplay/freeplay.component';
import { ArticleComponent } from './main-screen/game/main-menu/freeplay/article/article.component';
import { RandomTextComponent } from './main-screen/game/main-menu/freeplay/random-text/random-text.component';
import { SelectItemComponent } from './elements/select-item/select-item.component';
import { GameComponent } from './main-screen/game/game.component';
import { GameService } from './services/game.service';
import { PauseMenuComponent } from './main-screen/game/pause-menu/pause-menu.component';
import { StatService } from './services/stat.service';
import { SummaryComponent } from './main-screen/summary/summary.component';
import { EditComponent } from './main-screen/user-panel/edit/edit.component';
import { GamesComponent } from './main-screen/user-panel/games/games.component';
import { AccountSummaryComponent } from './main-screen/user-panel/account-summary/account-summary.component';
import { ArticleService } from './services/article.service';
import { WikiParser } from './libs/wiki-parser';
import { TextPreviewDialogComponent } from './dialogs/text-preview-dialog/text-preview-dialog.component';
import { SelectDialogComponent } from './dialogs/select-dialog/select-dialog.component';
import { AssetController } from './libs/asset-controller';
import { ErrorDialogComponent } from './dialogs/error-dialog/error-dialog.component';
import { OfficialChallengesComponent } from './main-screen/game/main-menu/challenges/official-challenges/official-challenges.component';
import { CustomChallengesComponent } from './main-screen/game/main-menu/challenges/custom-challenges/custom-challenges.component';
import { EditChallengesComponent } from './main-screen/game/main-menu/challenges/edit-challenges/edit-challenges.component';
import { ChallengeService } from './services/challenge.service';
import { EditChallengeDialogComponent } from './dialogs/edit-challenge-dialog/edit-challenge-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { Parser } from './libs/parser';

export function setupLanguageFactory(
  service: LanguageService): Function {
  return () => service.use(LanguageService.DEFAULT_LANGUAGE);
}

@NgModule({
  declarations: [
    AppComponent,
    BackgroundComponent,
    FloatingKeyComponent,
    MainScreenComponent,
    UserPanelComponent,
    LoginComponent,
    RegisterComponent,
    PanelComponent,
    MainMenuComponent,
    TranslatePipe,
    SplitPaneComponent,
    ChallengesComponent,
    FreeplayComponent,
    ArticleComponent,
    RandomTextComponent,
    SelectItemComponent,
    GameComponent,
    PauseMenuComponent,
    SummaryComponent,
    EditComponent,
    GamesComponent,
    AccountSummaryComponent,
    TextPreviewDialogComponent,
    SelectDialogComponent,
    ErrorDialogComponent,
    OfficialChallengesComponent,
    CustomChallengesComponent,
    EditChallengesComponent,
    EditChallengeDialogComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ClickOutsideModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatProgressBarModule,
    MatDialogModule,
    MatCheckboxModule,
    FormsModule
  ],
  providers: [
    UserService,
    ArticleService,
    ChallengeService,
    AuthGuardService,
    GameService,
    StatService,
    LanguageService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupLanguageFactory,
      deps: [LanguageService],
      multi: true
    },
    Parser,
    WikiParser,
    AssetController
  ],
  entryComponents: [
    TextPreviewDialogComponent,
    SelectDialogComponent,
    ErrorDialogComponent,
    EditChallengeDialogComponent,
    ConfirmDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
