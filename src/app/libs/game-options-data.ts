import { KeyboardLayout } from "./keyboard-layout";

export interface GameOptionsData {
  keyboardSkin: string,
  keyboardLayout: string
}
