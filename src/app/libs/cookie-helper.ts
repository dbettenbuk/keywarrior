export class CookieHelper {
    private constructor() { }

    public static setCookie(name: string, value: string, expires: number): void {
        var d = new Date();
        d.setTime(d.getTime() + (expires * 24 * 60 * 60 * 1000));
        var exp = "expires=" + d.toUTCString();
        document.cookie = name + "=" + value + ";" + exp + ";path=/";
    }

    public static getCookie(name: string): string {
        var cookieName = name + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return "";
    }

    public static cookieExists(name: string): boolean {
        return CookieHelper.getCookie(name) != "";
    }
}