import { KeyboardLayout } from "./keyboard-layout";

export class Challenge {

    private static DifficultyNames = [
        'DIFFICULTY_EASY',
        'DIFFICULTY_MEDIUM',
        'DIFFICULTY_HARD'
    ];

    public get id(): string {
        return this._id;
    }

    constructor(
        private _id: string,
        public name: string,
        public text: string,
        public timeLimit: number,
        public difficulty: number,
        public isOfficial: boolean,
        public ownerName: string
    ) { }

    public getFormattedTimeLimit(): string {
        return Math.floor(this.timeLimit / 60).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + ':' + Math.floor(this.timeLimit % 60).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })
    }

    public getDifficultyName(): string {
        return Challenge.DifficultyNames[this.difficulty];
    }

    public static createEmpty() {
        return new Challenge('', '', '', 0, 0, false, '');
    }
}
