import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class Parser {
    private static mappings = [
        { filter: /(\(([^()])*\))/g, replace: '' },
        { filter: /(?:\u0024|\u2013|\u0022)/g, replace: '' },
        { filter: /[\r\n]+/g, replace: ' ' },
        { filter: /\u2013/g, replace: '-' },
        { filter: /\u0027/g, replace: '' },
        { filter: /\u0025/g, replace: '' },
        { filter: / {2,}/g, replace: ' ' },
        { filter: /\//g, replace: ' ' },
        { filter: /\\/g, replace: ' ' },
        { filter: / \./g, replace: '.' },
        { filter: / \,/g, replace: ',' },
        { filter: /\;/g, replace: ',' },
        { filter: /\:/g, replace: ',' }
    ];

    public parseText(text: string): string {
        let parsed = text;
        // Workaround for removing nested parentheses and their content (JS doesn't support the recursive flag of Regex)
        while (parsed.match(Parser.mappings[0].filter)) {
            parsed = parsed.replace(Parser.mappings[0].filter, '');
        }

        for (let mapping of Parser.mappings) {
            parsed = parsed.replace(mapping.filter, mapping.replace);
        }

        return parsed;
    }
}
