import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AssetController {
  public readonly KeyboardLayouts = [
    {
      key: 'en-gb',
      value: 'CAPTION_LAYOUT_ENGLISH_UK'
    },
    {
      key: 'hu-hu',
      value: 'CAPTION_LAYOUT_HUNGARIAN'
    }
  ];
  public readonly KeyboardSkins = [
    {
      key: 'classic',
      value: 'Classic'
    },
    {
      key: 'light',
      value: 'Light'
    },
    {
      key: 'dark',
      value: 'Dark Neon'
    }
  ];
}
