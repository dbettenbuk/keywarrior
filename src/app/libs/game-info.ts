export enum ScoreRank {
    Bad,
    Okay,
    Good,
    Excellent
};

export class GameInfo {

    public get score(): number {
        return this._score;
    }

    public get percentage(): number {
        return Math.floor(this._correctHits / (this._correctHits + this._wrongHits) * 100);
    }

    public get scoreRank(): ScoreRank {
        if (this.stars <= 1) {
            return ScoreRank.Bad;
        }
        return <ScoreRank>(this.stars - 2);
    }

    public get textTitle(): string {
        return this._textTitle;
    }

    public get difficulty(): number {
        return this._difficulty;
    }

    public get stars(): number {
        return Math.ceil(this.percentage / 20);;
    }

    public get playedAt(): Date {
        return this._playedAt;
    }

    public get elapsedTime(): number {
        return this._elapsedTime;
    }

    public get averageCpm(): number {
        return this._averageCpm;
    }

    public get highestStreak(): number {
        return this._highestStreak;
    }

    public get userId(): string {
        return this._userId;
    }

    public constructor(
        private _score: number,
        private _textTitle: string,
        private _difficulty: number,
        private _correctHits: number,
        private _wrongHits: number,
        private _playedAt: Date,
        private _elapsedTime: number,
        private _averageCpm: number,
        private _highestStreak: number,
        private _userId: string) {
    }
}