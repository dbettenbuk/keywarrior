export enum KeyType {
    KEYBOARD = "keyboard-key",
    FLOATING = "floating-key"
}

export class Key {
    private _element: HTMLDivElement;
    private _container: HTMLDivElement;
    private _content: string;
    private _type: KeyType;

    public get element(): HTMLDivElement {
        return this._element;
    }

    public get container(): HTMLDivElement {
        return this._container;
    }

    public get content(): string {
        return this._content;
    }

    public get type(): KeyType {
        return this._type;
    }

    constructor(content: string, type: KeyType) {
        this._content = content;
        this._type = type;

        this._element = document.createElement('div');
        this._element.innerHTML = '<span>' + content + '</span>';
        this._element.classList.add('key');
        this._element.setAttribute('input', content);

        if (type === KeyType.FLOATING) {
            this._container = document.createElement('div');
            this._container.classList.add('key-container');
            if (content === ' ') {
                this._container.classList.add('space-container');
            }

            this._container.classList.add(Key.getClassFor(content));
            this._container.classList.add('floating-key');
            this._container.appendChild(this._element);
        }
        else {
            this._element.classList.add(Key.getClassFor(content));
            this._element.classList.add('keyboard-key');
        }

    }

    public static getClassFor(keyContent: string): string {
        if (keyContent.length !== 1) {
            return 'key-' + keyContent.toLowerCase();
        }
        return 'key-' + keyContent.toLowerCase().charCodeAt(0);
    }
}
