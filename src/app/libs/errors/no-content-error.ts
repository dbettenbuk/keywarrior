export class NoContentError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'NoContentError';

        Object.setPrototypeOf(this, NoContentError.prototype);
    }
}