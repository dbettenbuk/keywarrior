export class EmptyContentError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'EmptyContentError';

        Object.setPrototypeOf(this, EmptyContentError.prototype);
    }
}
