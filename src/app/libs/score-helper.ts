export class ScoreHelper {
    public static SCORE_PER_KEY: number = 5;
    public static SCORE_PER_SPECIAL_CHARACTER: number = 8;
    public static MAX_MULTIPLIER: number = 4;

    private constructor() { }

    public static getScoreForChar(char: string): number {
        let code = char.charCodeAt(0);
        let score = ScoreHelper.SCORE_PER_SPECIAL_CHARACTER;
        // Lower-case, upper-case letters and numbers
        if (code >= 48 && code <= 57 ||
            code >= 65 && code <= 90 ||
            code >= 97 && code <= 122) {
            score = ScoreHelper.SCORE_PER_KEY;
        }
        // Space
        else if (code === 32) {
            score = 2;
        }

        return score;
    }

    public static getScoreForText(text: string): number {
        let score = 0;
        for (let char of text) {
            score += ScoreHelper.getScoreForChar(char) * ScoreHelper.MAX_MULTIPLIER;
        }

        return score;
    }
}