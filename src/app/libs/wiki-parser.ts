import * as $ from 'jquery';
import { Injectable } from '@angular/core';
import { NoContentError } from './errors/no-content-error';
import { EmptyContentError } from './errors/empty-content-error';
import { Parser } from './parser';

@Injectable({
    providedIn: 'root'
})
export class WikiParser extends Parser {
    constructor() {
        super();
    }

    public parseArticle(layoutPrefix: string, article: string, callback): void {
        let wikiApi = "https://" + layoutPrefix + ".wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=" + article + "&callback=?";
        $.getJSON(wikiApi, {
            format: "json"
        })
            .done((data) => {
                let content = "";
                let error: Error = null;
                if (!data || !data.query || !data.query.pages || !Object.keys(data.query.pages)[0]) {
                    error = new NoContentError('');
                } else {
                    content = data.query.pages[Object.keys(data.query.pages)[0]]['extract'];
                    if (content && content.length > 0) {
                        content = this.parseText(content);
                    }
                    else {
                        error = new EmptyContentError('');
                    }
                }
                callback(content, error);
            });
    }

    private insert(target: string, source: string, index: number): string {
        return target.substr(0, index) + source + target.substr(index);
    }
}
