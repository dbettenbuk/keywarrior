import { Key, KeyType } from "./key";
import { range } from "rxjs";
import { KeyboardLayout } from "./keyboard-layout";

export class KeyFactory {
  public createKey(content: string, keyType: KeyType, skin: string): Key {
    let key = new Key(content, keyType);
    key.element.style.backgroundImage = 'url(assets/keyboard/' + skin + '/key.png)';
    if (key.content === ' ') {
      key.element.style.backgroundImage = 'url(assets/keyboard/' + skin + '/space.png)';
    }
    return key;
  }

  public createKeyboard(keyCodes: Array<number>, skin: string): Array<Key> {
    let keys = new Array<Key>();
    for (let keyCode of keyCodes) {
      keys.push(this.createKey(String.fromCharCode(keyCode), KeyType.KEYBOARD, skin));
    }

    keys.push(this.createKey(' ', KeyType.KEYBOARD, skin));
    keys.push(this.createKey(',', KeyType.KEYBOARD, skin));
    keys.push(this.createKey('.', KeyType.KEYBOARD, skin));
    keys.push(this.createKey('-', KeyType.KEYBOARD, skin));
    keys.push(this.createKey('shift', KeyType.KEYBOARD, skin));

    return keys;
  }

  public getCodesFor(keyboardLayoutKey: string): Array<number> {
    let array = new Array<number>();
    range(48, 10).subscribe((value) => {
      array.push(value);
    });
    range(97, 26).subscribe((value) => {
      array.push(value);
    });

    // Handle other layouts
    if (keyboardLayoutKey === 'hu-hu') {
      array.push(252, 243, 337, 250, 369, 233, 225, 246);
    }

    return array;
  }
}
