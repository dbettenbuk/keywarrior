export enum ArticleLength {
    LONG = "Long",
    SHORT = "Short"
}

export class Article {

    public get name(): string {
        return this._name;
    }

    public get url(): string {
        return this._url;
    }

    public get length(): ArticleLength {
        return this._length;
    }

    constructor(
        private _name: string,
        private _url: string,
        private _length: ArticleLength
    ) { }
}
