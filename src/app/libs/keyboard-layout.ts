export class KeyboardLayout {
  public static readonly ENGLISH_UK = new KeyboardLayout('en-gb', 'English (United Kingdom)');
  public static readonly HUNGARIAN = new KeyboardLayout('hu-hu', 'Hungarian');

  constructor(
    private key: string,
    private fullName: string
  ) { }
}
