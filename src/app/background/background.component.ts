import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.scss']
})
export class BackgroundComponent implements OnInit {

  private static readonly KEY_COUNT: number = 15;

  public get keyCount(): number {
    return BackgroundComponent.KEY_COUNT;
  }

  constructor() { }

  public ngOnInit(): void {
  }
}
