import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloatingKeyComponent } from './floating-key.component';

describe('FloatingKeyComponent', () => {
  let component: FloatingKeyComponent;
  let fixture: ComponentFixture<FloatingKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloatingKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatingKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
