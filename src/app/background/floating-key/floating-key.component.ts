import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-floating-key',
  templateUrl: './floating-key.component.html',
  styleUrls: ['./floating-key.component.scss']
})
export class FloatingKeyComponent implements AfterViewInit {

  @ViewChild('keyDiv')
  private keyDiv: ElementRef<HTMLDivElement>;

  constructor() { }

  public ngAfterViewInit(): void {
    this.keyDiv.nativeElement.style.left = Math.floor((Math.random() * window.innerWidth - 200) + 100) + 'px';
    this.keyDiv.nativeElement.style.top = Math.floor((Math.random() * window.innerHeight - 200) + 100) + 'px';

    const keySize = Math.floor((Math.random() * 70) + 20);

    this.keyDiv.nativeElement.style.width = keySize + 'px';
    this.keyDiv.nativeElement.style.height = keySize + 'px';
    this.keyDiv.nativeElement.style.backgroundSize = keySize + 'px';
    this.keyDiv.nativeElement.style.opacity = Math.random().toString();

    this.startAnimation(this.keyDiv.nativeElement);
  }

  private startAnimation(element): void {

    const seed = Math.random();

    const elementOpacity = parseFloat(element.style.opacity);
    const targetOpacity = 0;

    const duration: number = Math.floor((Math.random() * 10000) + 5000);

    if (seed < 0.5) {
      Reflect.apply(element.animate, element, [[
        {
          top: element.style.top,
          opacity: element.style.opacity
        },
        {
          top: Math.floor((Math.random() * window.innerHeight - 200) + 100) + 'px',
          opacity: targetOpacity.toString()
        }], duration]);
    } else {
      Reflect.apply(element.animate, element, [[
        {
          left: element.style.left,
          opacity: element.style.opacity
        },
        {
          left: Math.floor((Math.random() * window.innerWidth - 200) + 100) + 'px',
          opacity: targetOpacity.toString()
        }], duration]);
    }

    setTimeout(() => {
      this.startAnimation(element);
    }, duration + 100);
  }
}
