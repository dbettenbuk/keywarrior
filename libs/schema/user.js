const Schema = require('mongoose').Schema;
const passportLocalMongoose = require('passport-local-mongoose');

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  token: {
    type: String
  },
  isAdmin: {
    type: Boolean
  },
  username: {
    type: String,
    unique: true
  },
  level: {
    type: Number,
    default: 1
  },
  xp: {
    type: Number,
    default: 0
  },
  highestScore: {
    type: Number
  },
  bestCpm: {
    type: Number
  },
  highestStreak: {
    type: Number
  },
  registeredAt: {
    type: Date
  }
});

UserSchema.plugin(passportLocalMongoose, {
  usernameLowerCase: true,
});

// Compile model from schema
module.exports = UserSchema;
