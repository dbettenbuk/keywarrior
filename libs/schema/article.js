const Schema = require('mongoose').Schema;

const ArticleSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  length: {
    type: String,
    required: true,
    enum: ['Short', 'Long']
  }
});

module.exports = ArticleSchema;
