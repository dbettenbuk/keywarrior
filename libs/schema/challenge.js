const Schema = require('mongoose').Schema;

const ChallengeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  timeLimit: {
    type: Number,
    required: true
  },
  difficulty: {
    type: Number,
    required: true
  },
  isOfficial: {
    type: Boolean,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true
  }
});

module.exports = ChallengeSchema;
