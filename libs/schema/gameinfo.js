const Schema = require('mongoose').Schema;

const GameInfoSchema = new Schema({
  score: {
    type: Number,
    required: true
  },
  textTitle: {
    type: String,
    required: true
  },
  difficulty: {
    type: Number,
    required: true
  },
  correctHits: {
    type: Number,
    required: true
  },
  wrongHits: {
    type: Number,
    required: true
  },
  playedAt: {
    type: Date,
    required: true
  },
  elapsedTime: {
    type: Number,
    required: true
  },
  averageCpm: {
    type: Number,
    required: true
  },
  highestStreak: {
    type: Number,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true
  }
});

// Compile model from schema
module.exports = GameInfoSchema;
