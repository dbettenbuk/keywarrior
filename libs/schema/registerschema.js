const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://keywarrior:zdqUhNwPWczi3QJm@keywarriordb-wx1xw.mongodb.net/keywarrior');

const UserSchema = require('./user');
const GameInfoSchema = require('./gameinfo');
const ArticleSchema = require('./article');
const ChallengeSchema = require('./challenge');

module.exports = () => {
  mongoose.model('users', UserSchema);
  mongoose.model('games', GameInfoSchema);
  mongoose.model('articles', ArticleSchema);
  mongoose.model('challenges', ChallengeSchema);
};
