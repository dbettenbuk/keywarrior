const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const UserController = require('../controller/usercontroller');

const User = mongoose.model('users');

module.exports = function (app, passport) {
  router.post('/register', (request, response) => {
    UserController.register(request.body.username, request.body.email, request.body.password).then((user) => {
      response.json(user);
    }, (error) => {
      console.log(error);
      response.status(400).send().end();
    });
  });

  router.post('/login', passport.authenticate('local', {
    session: false
  }), (request, response) => {
    UserController.login(request.user).then((user) => {
      response.json(user).end();
    }, (error) => {
      console.log(error);
      response.status(400).end();
    });
  });

  router.get('/logout', function (req, res) {
    res.status(200).send().end();
  });

  router.post('/auth', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    response.json(request.user).status(200).end();
  });

  router.post('/modify', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    UserController.modify(request.user, request.body).then(() => {
      response.status(200).send().end();
    }, (error) => {
      console.log(error);
      response.status(404).send().end();
    })
  });

  router.post('/admin', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    if (request.user && request.user.isAdmin) {
      response.status(200).end();
    } else {
      response.status(403).end();
    }
  });

  router.post('/addgame', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    UserController.addGame(request.body.gameInfo).then(() => {
      response.status(200).send().end();
    }, error => {
      console.log(error);
      response.status(403).send().end();
    });
  });

  router.get('/games', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    UserController.getAllGamesFor(request.user).then((games) => {
      response.json(games).status(200).send().end();
    }, error => {
      console.log(error);
      response.status(403).send().end();
    });
  });

  router.get('/stats', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    UserController.getStatsFor(request.user).then((data) => {
      response.status(200).json(data).end();
    }, (error) => {
      console.log(error);
      response.status(404).send().end();
    });
  });

  router.post('/validatescore', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    UserController.updateXp(request.user, request.body.score).then((leveledUp) => {
      response.json(leveledUp).end();
    }, (error) => {
      console.log(error);
      response.status(403).send().end();
    });
  });

  app.use("/rest/users", router);
};
