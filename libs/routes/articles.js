const express = require('express');
const router = express.Router();
const ArticleController = require('../controller/articlecontroller');

module.exports = function (app, passport) {
  router.get('/', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    ArticleController.getAllArticles().then((articles) => {
      response.json(articles).end();
    }, (error) => {
      console.log(error);
      response.status(404).end();
    });
  });

  app.use("/rest/articles", router);
};