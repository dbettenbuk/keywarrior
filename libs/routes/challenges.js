const express = require('express');
const router = express.Router();
const ChallengeController = require('../controller/challengecontroller');

module.exports = function (app, passport) {
  router.get('/', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    ChallengeController.getChallengesFor(request.user).then((challenges) => {
      response.json(challenges).end();
    }, (error) => {
      console.log(error);
      response.status(404).end();
    });
  });

  router.get('/official', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    ChallengeController.getOfficialChallenges().then((challenges) => {
      response.json(challenges).end();
    }, (error) => {
      console.log(error);
      response.status(404).end();
    });
  });

  router.get('/custom', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    ChallengeController.getCustomChallenges().then((challenges) => {
      response.json(challenges).end();
    }, (error) => {
      console.log(error);
      response.status(404).end();
    });
  });

  router.post('/add', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    if (request.body.challenge.isOfficial && !request.user.isAdmin) {
      response.status(403).end();
    } else {
      ChallengeController.addChallenge(request.body.challenge, request.user._id).then(() => {
        response.status(200).end();
      }, (error) => {
        console.log(error);
        response.status(403).end();
      });
    }
  })

  router.put('/modify', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    if (request.body.challenge.isOfficial && !request.user.isAdmin) {
      response.status(403).end();
    } else {
      ChallengeController.modifyChallenge(request.body.challenge).then(() => {
        response.status(200).end();
      }, (error) => {
        console.log(error);
        response.status(403).end();
      });
    }
  });

  router.delete('/delete/:id', passport.authenticate('bearer', {
    session: false
  }), (request, response) => {
    ChallengeController.deleteChallenge(request.params.id, request.user._id).then(() => {
      response.status(200).end();
    }, (error) => {
      response.status(403).end();
    });
  });

  app.use("/rest/challenges", router);
};
