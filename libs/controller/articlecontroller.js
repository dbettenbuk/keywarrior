const mongoose = require('mongoose');
const Article = mongoose.model('articles');

class ArticleController {
    getAllArticles() {
        return new Promise((resolve, reject) => {
            Article.find((error, articles) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(articles);
                }
            });
        });
    }
}

module.exports = new ArticleController();