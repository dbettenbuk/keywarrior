const mongoose = require('mongoose');
const User = mongoose.model('users');
const crypto = require('crypto');
const GameInfo = mongoose.model('games');
const LevelHelper = require('../util/levelhelper');

class UserController {

  register(username, email, password) {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(32, function (ex, buf) {
        const token = buf.toString('hex');
        User.register(new User({
          username: username,
          email: email,
          token: token,
          isAdmin: false,
          registeredAt: new Date()
        }), password, (error, user) => {
          if (error) {
            reject(error);
          } else {
            resolve(user);
          }
        });
      });
    });
  }

  login(user) {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(32, function (ex, buf) {
        const token = buf.toString('hex');
        User.update({
            _id: user._id
          }, {
            token: token
          },
          (error, affected, response) => {
            if (error) {
              reject(error);
            } else {
              user.token = token;
              resolve(user);
            }
          });
      });
    });
  }

  addGame(gameInfo) {
    return new Promise((resolve, reject) => {
      const gameInfoDocument = new GameInfo({
        score: gameInfo._score,
        textTitle: gameInfo._textTitle,
        difficulty: gameInfo._difficulty,
        correctHits: gameInfo._correctHits,
        wrongHits: gameInfo._wrongHits,
        playedAt: gameInfo._playedAt,
        elapsedTime: gameInfo._elapsedTime,
        averageCpm: gameInfo._averageCpm,
        highestStreak: gameInfo._highestStreak,
        user: gameInfo._userId
      });

      User.findOne({
        _id: gameInfo._userId
      }, (error, user) => {
        if (error) {
          reject(error);
        } else {
          let highestScore = gameInfo._score > user.highestScore ? gameInfo._score : user.highestScore;
          let highestStreak = gameInfo._highestStreak > user.highestStreak ? gameInfo._highestStreak : user.highestStreak;
          let bestCpm = gameInfo._averageCpm > user.bestCpm ? gameInfo._averageCpm : user.bestCpm;

          User.update({
            _id: gameInfo._userId
          }, {
            highestScore: highestScore,
            highestStreak: highestStreak,
            bestCpm: bestCpm
          }, (error, affected, response) => {
            if (error) {
              reject(error);
            } else {
              gameInfoDocument.save((error) => {
                if (error) {
                  reject(error);
                } else {
                  resolve();
                }
              });
            }
          });
        }
      });
    });
  }

  getAllGamesFor(user) {
    return new Promise((resolve, reject) => {
      GameInfo.find({
        user: user._id
      }, (error, games) => {
        if (error) {
          reject(error);
        } else {
          resolve(games);
        }
      });
    });
  }

  getStatsFor(user) {
    return new Promise((resolve, reject) => {
      User.findOne({
        _id: user._id
      }, (err, user) => {
        if (err) {
          reject(err);
        } else {
          resolve({
            level: user.level,
            xp: user.xp,
            xpForCurrentLevel: LevelHelper.xpBuffer[user.level],
            xpForNextLevel: LevelHelper.xpBuffer[user.level + 1],
            highestScore: user.highestScore,
            highestStreak: user.highestStreak,
            bestCpm: user.bestCpm
          });
        }
      });
    });
  }

  modify(user, data) {
    return new Promise((resolve, reject) => {
      User.update({
          _id: user._id
        }, {
          email: data.email
        },
        (error, affected, response) => {
          if (error) {
            reject(error);
          } else {
            user.setPassword(data.password, () => {
              user.save();
              resolve();
            });
          }
        });
    });
  }

  updateXp(user, score) {
    return new Promise((resolve, reject) => {
      let levelUp = false;
      let newLevel = user.level;
      if (user.xp + score > LevelHelper.xpBuffer[newLevel + 1]) {
        newLevel++;
        levelUp = true;
      }
      User.update({
        _id: user._id
      }, {
        xp: user.xp + score,
        level: newLevel
      }, (error, affected, response) => {
        if (error) {
          reject(error);
        } else {
          resolve({
            leveledUp: levelUp,
            newLevel: newLevel
          });
        }
      });
    });
  }
}

module.exports = new UserController();
