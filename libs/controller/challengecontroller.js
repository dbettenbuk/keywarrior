const mongoose = require('mongoose');
const Challenge = mongoose.model('challenges');

class ChallengeController {

  getChallenges(condition) {
    return new Promise((resolve, reject) => {
      Challenge.find(condition, (error, challenges) => {
        if (error) {
          reject(error);
        } else {
          resolve(challenges);
        }
      });
    });
  }

  getChallengesWithOwner(condition) {
    return new Promise((resolve, reject) => {
      Challenge.find(condition).populate('userId').exec((error, challenges) => {
        if (error) {
          reject(error);
        } else {
          resolve(challenges);
        }
      });
    });
  }

  getOfficialChallenges() {
    return this.getChallengesWithOwner({
      isOfficial: true
    });
  }

  getCustomChallenges() {
    return this.getChallengesWithOwner({
      isOfficial: false
    });
  }

  getChallengesFor(user) {
    return this.getChallenges({
      user: user._id
    });
  }

  addChallenge(challenge, ownerId) {
    return new Promise((resolve, reject) => {
      const challengeDocument = new Challenge({
        name: challenge.name,
        text: challenge.text,
        timeLimit: challenge.timeLimit,
        difficulty: challenge.difficulty,
        isOfficial: challenge.isOfficial,
        user: ownerId
      });

      challengeDocument.save((error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  modifyChallenge(challenge) {
    return new Promise((resolve, reject) => {
      Challenge.update({
        _id: challenge._id
      }, {
        name: challenge.name,
        text: challenge.text,
        timeLimit: challenge.timeLimit,
        difficulty: challenge.difficulty,
        isOfficial: challenge.isOfficial
      }, (error, affected, response) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  deleteChallenge(id, userId) {
    return new Promise((resolve, reject) => {
      Challenge.deleteOne({
        _id: id,
        user: userId
      }, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }
}

module.exports = new ChallengeController();
