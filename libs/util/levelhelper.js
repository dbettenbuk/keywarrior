const MAX_LEVEL = 1000;

class LevelHelper {
    constructor() {
        this.xpBuffer = Array();
        this.xpBuffer[1] = 0;
        for (let i = 2; i < MAX_LEVEL; i++) {
            this.xpBuffer[i] = Math.floor(2000 + 1.235 * Math.log(i) + 1.023 * this.xpBuffer[i - 1]);
        }
    }

    getLevelForXp(xp) {
        for (let i = 1; xp < this.xpBuffer[i] && i < MAX_LEVEL; i++);
        return i;
    }
}

module.exports = new LevelHelper();